import { useRef, useState } from "react";
import { FileEarmarkMusicFill, PauseFill, PlayCircleFill, PlayFill } from "react-bootstrap-icons";

export default function MusicPlayer() {
	const audioRef = useRef<HTMLAudioElement>(null);
	const progressContainerRef = useRef<HTMLDivElement>(null);
	const [playing, setPlaying] = useState<boolean>(false);
	const [progress, setProgress] = useState<number>(0.0);

	const togglePlay = (event) => {
		setPlaying(!playing);
		if (playing) {
			audioRef.current.pause();
		} else {
			audioRef.current.play();
		}
		
	}
	
	return (
		<div className="flex fixed inset-x-0 bottom-0 z-40 flex-row gap-x-4 items-center p-4 h-16 bg-gray-50 border border-gray-300 shadow-md">
			<button onClick={togglePlay}>
				{playing ? (
					<PauseFill className="w-10 h-10 fill-black" />
				) : (
					<PlayFill className="w-10 h-10 fill-black" />
				)}
			</button>
			<audio
				id="music"
				className="hidden"
				ref={audioRef}
				onTimeUpdate={event => {
					setProgress(audioRef.current.currentTime / audioRef.current.duration);
				}}
				onPause={() => setPlaying(false)}
				onPlay={() => setPlaying(true)}>
				<source src="/music/stillalive.mp3" type="audio/mpeg" />
				Your browser does not support the audio element.
			</audio>
			<div
				className="flex flex-row h-2 bg-gray-300 rounded-sm grow"
				ref={progressContainerRef}
				onClick={event => {
					const progressValue =
						(event.clientX -
							progressContainerRef.current.getBoundingClientRect().left) /
						(progressContainerRef.current.getBoundingClientRect().width);
					setProgress(progressValue),
					audioRef.current.currentTime = audioRef.current.duration * progressValue
				}}>
				<div
					className="h-full bg-red-600 rounded-sm duration-150"
					style={{
						width: `${progress * 100}%`,
					}}></div>
				<div className="flex justify-center items-center w-5 h-5 bg-white p-1 hover:p-1.5 duration-200 rounded-full border -translate-x-2 -translate-y-1.5">
					<div className="w-full h-full bg-rose-500 rounded-full"></div>
				</div>
			</div>
			<div>
				{/* By Sam Herbert (@sherb), for everyone. More @ http://goo.gl/7AJzbL */}
				<svg viewBox="0 0 55 80" xmlns="http://www.w3.org/2000/svg" fill="currentColor" className="ml-2 w-8 h-8 fill-rose-600">
					<g transform="matrix(1 0 0 -1 0 80)">
						<rect width="10" height="20" rx="3">
							<animate attributeName="height"
								begin="0s" dur="4.3s"
								values="20;45;57;80;64;32;66;45;64;23;66;13;64;56;34;34;2;23;76;79;20" calcMode="linear"
								repeatCount="indefinite" />
						</rect>
						<rect x="15" width="10" height="80" rx="3">
							<animate attributeName="height"
								begin="0s" dur="2s"
								values="80;55;33;5;75;23;73;33;12;14;60;80" calcMode="linear"
								repeatCount="indefinite" />
						</rect>
						<rect x="30" width="10" height="50" rx="3">
							<animate attributeName="height"
								begin="0s" dur="1.4s"
								values="50;34;78;23;56;23;34;76;80;54;21;50" calcMode="linear"
								repeatCount="indefinite" />
						</rect>
						<rect x="45" width="10" height="30" rx="3">
							<animate attributeName="height"
								begin="0s" dur="2s"
								values="30;45;13;80;56;72;45;76;34;23;67;30" calcMode="linear"
								repeatCount="indefinite" />
						</rect>
					</g>
				</svg>
			</div>
		</div>
	);
}

export function MusicWidget({ setOpen }) {
	return (
		<button className="flex fixed right-5 bottom-5 z-30 justify-center items-center p-2 rounded-md" onClick={() => setOpen(true)}>
			<FileEarmarkMusicFill className="w-8 h-8 text-black" />
		</button>
	)
}