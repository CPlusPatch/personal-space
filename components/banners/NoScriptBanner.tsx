import { ExclamationTriangleIcon } from "@heroicons/react/24/outline";

export default function NoScriptBanner() {
	return (
		<noscript>
			<div className="fixed inset-x-0 bottom-0 pb-2 sm:pb-5">
				<div className="px-2 mx-auto max-w-7xl sm:px-6 lg:px-8">
					<div className="p-2 bg-orange-600 rounded-lg shadow-lg sm:p-3">
						<div className="flex flex-wrap justify-between items-center">
							<div className="flex flex-1 items-center w-0">
								<span className="flex p-2 bg-orange-800 rounded-lg">
									<ExclamationTriangleIcon
										className="w-6 h-6 text-white"
										aria-hidden="true"
									/>
								</span>
								<p className="flex flex-col ml-3 font-medium text-white truncate font-inter">
									<span className="md:hidden">
										Hey! Please enable JavaScript!
									</span>
									<h4 className="hidden font-extrabold md:inline">
										Hey! Please enable JavaScript!
									</h4>
									<span className="hidden md:inline">
										There are no trackers and the code for this
										website is fully GPL licensed!
									</span>
								</p>
							</div>
							<div className="flex-shrink-0 order-3 mt-2 w-full sm:order-2 sm:mt-0 sm:w-auto">
								<a
									href="https://codeberg.org/CPlusPatch/cpluspatch-cms-next"
									className="flex justify-center items-center px-4 py-2 text-sm font-medium text-orange-600 bg-white rounded-md border border-transparent shadow-sm hover:bg-indigo-50">
									Source code
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</noscript>
	);
}
