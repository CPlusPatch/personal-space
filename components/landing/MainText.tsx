import Button from "components/buttons/Button";
import Image from "next/image";
import Link from "next/link";
import profileImage from "public/static/avatar.webp";
import { BoxArrowUpRight } from "react-bootstrap-icons";
import Tilt from "react-parallax-tilt";

const MainText = () => {
	return (
		<div className="relative pb-16 sm:pb-24">
			<div className="flex flex-col-reverse gap-y-6 px-4 mx-auto mt-8 max-w-7xl md:mt-16 md:flex-row sm:px-6 font-inter">
				<div className="flex flex-col gap-y-4 justify-center w-full h-full text-left md:mt-0 md:w-2/3">
					<h1 className="text-4xl font-black text-gray-900 dark:text-gray-300 sm:text-5xl md:text-6xl">
						<span className="block">Hi!</span>
						<span className="block pb-2 text-transparent bg-clip-text bg-gradient-to-r from-pink-300 via-purple-300 to-indigo-400">
							I&apos;m Jesse!
						</span>
					</h1>
					<h4 className="gap-x-2 text-xl font-inter">
						Hiii! I&apos;m Jesse, a
						<span className="inline-block mx-2 w-6 h-4 align-middle rounded bg-pan"></span>
						<span className="text-transparent bg-clip-text bg-pan-text">pansexual</span>
						<span className="inline-block mx-2 w-6 h-4 align-middle rounded bg-trans"></span>
						<span className="text-transparent bg-clip-text bg-trans-text">
							transgender
						</span>{" "}
						developer from
						<span className="inline-block mx-2 w-6 h-4 align-middle rounded bg-france"></span>
						<span className="text-transparent bg-clip-text bg-france-text">France</span>
						!
					</h4>
					<h4 className="flex flex-row gap-x-2 items-center text-xl font-inter">
						I like aerospace, technology, free software, federation and cute trans
						girls.
					</h4>

					<div className="flex flex-col gap-x-2 gap-y-2 md:flex-row">
						<Link href="https://cpluspatch.com">
							<Button
								style="gray"
								className="flex flex-row gap-x-2 items-center w-auto">
								<BoxArrowUpRight />
								Visit my business site!
							</Button>
						</Link>
						<Link href="https://akko.saturno.black/jesse">
							<Button
								style="gray"
								className="flex flex-row gap-x-2 items-center w-auto">
								<BoxArrowUpRight />
								Talk to me on Fedi!
							</Button>
						</Link>
						<Link href="https://matrix.to/#/@cpluspatch:matrix.cpluspatch.com">
							<Button
								style="gray"
								className="flex flex-row gap-x-2 items-center w-auto">
								<BoxArrowUpRight />
								Talk to me on Matrix!
							</Button>
						</Link>
					</div>
				</div>
				<div className="flex justify-center items-center w-full rotate-3 md:justify-end md:w-1/3">
					<Tilt
						tiltMaxAngleX={4}
						tiltMaxAngleY={4}
						className="overflow-hidden w-72 h-72 rounded-md shadow">
						<Image
							src={profileImage}
							sizes="(max-width: 768px) 90vw, 33vw"
							priority
							className="w-full h-full"
							alt="Avatar"
						/>
					</Tilt>
				</div>
			</div>
		</div>
	);
};

export default MainText;