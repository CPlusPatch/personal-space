import type { NextPage } from "next";
import Navbar from "components/nav/navbar";
import Footer from "components/footer/footer";
import MainText from "components/landing/MainText";
import { Account } from "types/types";
import MetaTags from "components/head/MetaTags";

const Home: NextPage = ({ account }: { account: Account | false }) => {
	return (
		<div className="relative min-h-screen bg-gray-50">
			<MetaTags title={`Jesse · UwU <3`} />
			<Navbar account={account} />
			<main>
				
			<MainText />
			</main>
			<Footer />
		</div>
	);
};

export default Home;
