import { ReactElement } from "react";
import { Database } from "./supabase";

type Component = {
	id: string;
	vendor: string;
	type: string;
	element: (DynamicComponentProps) => JSX.Element;
	props?: any;
};

type DynamicComponentProps = {
	editingMode?: boolean;
}

type Identity<T> = { [P in keyof T]: T[P] }
type Replace<T, K extends keyof T, TReplace> = Identity<Pick<T, Exclude<keyof T, K>> & {
    [P in K] : TReplace
}>

type Post = Database["public"]["Tables"]["posts"]["Row"];
type Account = Database["public"]["Tables"]["accounts"]["Row"];
type PostWithAccount = Replace<Post, "author", Account>

type Posts = Post[] | Record<string, never>;
type PostsWithAccount = PostWithAccount[] | Record<string, never>

type PostOrEmpty = Post | Record<string, never>;