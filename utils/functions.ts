/**
 * Checks if an object is empty (= to {})
 * @param obj The object to check
 * @returns True if the object is {}
 */
export function isEmpty(obj: any): boolean {
	return JSON.stringify(obj) === "{}" || JSON.stringify(obj) === "[]";
}

/**
 * Joins two sets of classNames with a space
 * @param classes Classes to join
 * @returns Joined classes
 */
export function classNames(...classes) {
	return classes.filter(Boolean).join(" ");
}

/**
* Will generate a random unique slug (https://xkcd.com/936/)
* @param {number} count The amount of random words in the slug
*/
export function generateRandomSlug(count: number = 4): string {
	const wordlist = require("public/static/wordlist.json");
	const wordlistLength = wordlist.length

	var words = []

	while (count-- > 0) {
		words.push(wordlist[Math.floor(Math.random() * wordlistLength)])
	}

	return words.join("-");
}